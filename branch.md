# Working with branch

## 1. Create a remote branch

### 1.1 Push the reference
git push origin master:refs/heads/branch-name

### 1.3 Create the remote branch
git branch --track branch-name origin/branch-name

### 1.4 Switch to the new branch
git checkout branch-name

## 2. Update a branch

### 2.2 Modify the base in master
git checkout master
... Modify & commit some code

### 2.2 Update a branch with master
git checkout branch-name
git merge master