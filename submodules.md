# Working with submodules

## 1. Add a submodule

git submodule add git_repo.git path/to/clone
Example : git submodule add git@bitbucket.org:hugeen/submodule.git my_submodule

git add .gitmodules
git add my_submodule
git commit -m "Add a submodule"
git push

## 2. Update a submodule

### 2.a Retrieves submodule updates

cd my_submodule
git checkout master
git pull

### 2.b Commit changes

cd ..
git add my_submodule
git commit -m "Update the submodule"
git push

### 2.c Update a submodule

cd my_submodule
git checkout master
... Make some changes
... Commit changes
git push
... Redo 2.b

## 3. Clone a repo with submodules

git clone git@bitbucket.org:hugeen/project-a-with-submodule.git
git submodule update --init

## 4. Update a submodule after a git pull

git pull
git submodule update --init